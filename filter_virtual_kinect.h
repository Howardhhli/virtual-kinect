#pragma once

#include "SurfaceMeshPlugins.h"
#include "SurfaceMeshHelper.h"
#include "RichParameterSet.h"

class filter_virtual_kinect : public SurfaceMeshFilterPlugin{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "virtual_kinect.plugin.starlab")
    Q_INTERFACES(FilterPlugin)

public:
    QString name() { return "Virtual Kinect"; }
    QString description() { return "Virtually scan the scene as Kinect by reading OpenGL buffers."; }

    void initParameters(RichParameterSet* pars);
    void applyFilter(RichParameterSet* pars);
};
