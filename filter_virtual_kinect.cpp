#include "filter_virtual_kinect.h"

#include "StarlabDrawArea.h"
#include "RenderObjectExt.h"

// OpenMP
#include <omp.h>

void filter_virtual_kinect::initParameters(RichParameterSet *pars)
{
    pars->addParam(new RichBool("viz", true, "Visualize"));
}

void filter_virtual_kinect::applyFilter(RichParameterSet *pars)
{
	int w = drawArea()->width();
	int h = drawArea()->height();

	// Read buffers
	GLfloat* depthBuffer = new GLfloat[w * h];
	glReadPixels(0, 0, w, h, GL_DEPTH_COMPONENT, GL_FLOAT, depthBuffer);
	GLubyte* colorBuffer = new GLubyte[w * h * 4];
	glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, colorBuffer);

	// save to images
	QImage debugImg(w, h, QImage::Format_ARGB32);
	QImage debugDepth(w, h, QImage::Format_ARGB32);
	for (int x = 0; x < w; x++){
		for (int y = 0; y < h; y++){
			uint dIndex = (y * w) + x;
			uint d = (uint)(depthBuffer[dIndex] * 255);
			debugDepth.setPixel(x, y, QColor(d, d, d).rgb());

			uint rgbIndex = dIndex * 4;
			uint r = (uint)colorBuffer[rgbIndex + 0];
			uint g = (uint)colorBuffer[rgbIndex + 1];
			uint b = (uint)colorBuffer[rgbIndex + 2];
			debugImg.setPixel(x, y, QColor(r, g, b).rgb());
		}
	}

	debugDepth.save("Depth.png");
	debugImg.save("RGB.png");

    drawArea()->updateGL();
}


